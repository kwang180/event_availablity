Time::DATE_FORMATS[:short_time] = '%-k:%M'

class Event < ApplicationRecord
  scope :openings, -> { where(kind: 'opening') }
  scope :appointments, -> { where(kind: 'appointment') }

  scope :openings_in_range, ->(days) {
    openings.where(starts_at: days)
            .or(where('weekly_recurring = ? AND starts_at < ?', true, days.last.end_of_day))
  }

  scope :day_appointments, ->(day) {
    appointments
      .where(starts_at: day.beginning_of_day..day.end_of_day)
      .order(:starts_at)
  }

  def self.availabilities(date)
    # dates range include all given date and the next 6 days
    days = date..(date + 6.days)

    # keep opening days range
    openings_in_range = openings_in_range(days)

    # begin looping through days
    days.map do |day|
      # selects opening events for loop day
      day_openings = openings_in_range.select do |opening|
        (opening.starts_at.to_date == day) ||
          (opening.starts_at.wday == day.wday && opening.weekly_recurring)
      end

      slots = []
      if day_openings.any?
        # fetchs appointments for loop day
        day_appointments = day_appointments(day)

        # loops through for day opening events
        slots = day_openings.each_with_object([]) do |opening, arr|
          # loops through 30 minutes time slots
          time = opening.starts_at
          while time < opening.ends_at
            # checks if there are any appointment at given time
            time_taken = day_appointments.any? do |appointment|
              # substract 1 minute to the appointment end time
              appointment_end_time = appointment.ends_at - 1.minute
              time_range = appointment.starts_at.to_s(:short_time)..appointment_end_time.to_s(:short_time)
              time_range.cover?(time.to_s(:short_time))
            end

            # adds time to slots array if not taken
            arr << time.to_s(:short_time) unless time_taken
            time += 30.minutes
          end
          arr
        end
      end

      {
        date: day.to_date,
        slots: slots || []
      }
    end
  end
end
